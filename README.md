# DND Master Development Environment Setup


### 

1. Edit vagrant file to add folder share location, i.e. code location.

    ```
    31 config.vm.synced_folder "./app", "/app"
    ```
> First arg is host, second is guest. (Shouldn't need to change guest)


2. Run cmd in shell from where the Vagrantfile is located

    ```
    $ vagrant up
    ```

3. Navigate to ```localhost:8080``` in browser
